import React, {Component, PropTypes} from 'react';
import ReactDOM from 'react-dom';
import {Tab, Tabs} from 'material-ui/Tabs';
import Dialog from 'material-ui/Dialog';
import FlatButton from 'material-ui/FlatButton';
import RaisedButton from 'material-ui/RaisedButton';
import Toggle from 'material-ui/Toggle';
import TimePicker from 'material-ui/TimePicker';
import SelectField from 'material-ui/SelectField';
import MenuItem from 'material-ui/MenuItem';
import {blue500} from 'material-ui/styles/colors';
import TextField from 'material-ui/TextField';
import Divider from 'material-ui/Divider';
import DropDown from './common/DropDown';
import {Editor, EditorState} from 'draft-js';
import FontIcon from 'material-ui/FontIcon';
import MaterialIcons from 'react-google-material-icons';


class TabsBuildingBlock extends Component {

  constructor() {
    super();
    this.state = {
      open: false,
      editorState: EditorState.createEmpty(),
      font_default: 1,
      font_size: 1
    };

  }
  handleOpen = () => {
    this.setState({open: true});
  };
  handleNext = () => {
    this.setState({open: false});
  };
  onChange = (editorState) => this.setState({editorState});
  handleChangeFont = (event, index, font_default) => this.setState({ font_default });
  handleChangeFontSize = (event, index, font_size) => this.setState({ font_size });
  render() {
    const menu_days = [ 1, 2, 3, 4, 5, 6,];
    const MessageTab = () => {
      return (
        <div className="email-form">
          <p>
            Email Template:   <DropDown title="days" items={menu_days} defaultText="days"/>
          </p>
          <p>
            From :
            <TextField hintText="help@mycommunity.com" underlineShow={false}/>
          </p>
          <Divider />
          <p style={{
            margin: 0
          }}>

            <TextField hintText="Enter your email subject" underlineShow={false}/>

          </p>
          <Divider />
              <Editor editorState={this.state.editorState} onChange={this.onChange} />
        </div>
      )
    }

    const DeliveryTab = () => {
      return (
          <div className="bodyContentTab">
            <div className="icon-part">
              <MaterialIcons icon="access_time" size={70} />
            </div>
              <div className="option-part">
            <strong>Delivery Window </strong> <span style={{ display: 'inline', float:'right'}}>
              <Toggle
            defaultToggled={true}
            style={styles.toggle}
          />
        </span>
            <p><span>Delivery your message at a time that best suits the recicept </span>

        </p>
            <Divider />
            <div id="week-date">
              <p>Choose a Delivery window ( recicept's timezone )</p>

              <input type="checkbox" /> Mon
              <input type="checkbox" /> Tue
              <input type="checkbox" /> Wed
              <input type="checkbox" /> Thur
              <input type="checkbox" /> Fri
              <input type="checkbox" /> Sat
              <input type="checkbox" /> Sun
            </div>
            <div id="select-time">
              <TimePicker
                style={{ display: 'inline', marginRight: 10}}
                textFieldStyle={{ borderWidth: 1, borderColor: '#ccc', borderRadius: 2, padding: 4 }}
                hintText="12hr Format"
              />
              to
              <TimePicker
                style={{ display: 'inline', marginLeft: 10}}
                textFieldStyle={{ borderWidth: 1, borderColor: '#ccc', borderRadius: 2, padding: 4 }}
                hintText="12hr Format"
              />
            </div>
          </div>
          </div>
      );
    }

    const InteractionTab = () => {
      return(
        <div>
            <div className="email-form">
              <p>Choose what actions people can take with this message.</p>
                <Divider />
                <div id="interaction-optoons">
                  <p><input type="checkbox" /> confirm when read</p>
                  <p><input type="checkbox" /> allow dismiss</p>
                  <p><input type="checkbox" /> allow snooze</p>
                </div>
              </div>
                  <div className="bodyContentTab">
                    <div className="icon-part">
                      <MaterialIcons icon="stars" size={70} />
                    </div>
                    <div className="option-part">
                      <strong>Goal </strong> <span style={{ display: 'inline', float:'right'}}>
                          <Toggle
                        defaultToggled={true}
                        style={styles.toggle}
                      />
                    </span>
                        <p>Set measumentable goal for this message</p>
                        <Divider />
                        <p><strong>This message will be successful if </strong></p>
                        <DropDown title="days" items={menu_days} defaultText="Select an attribute or event"/>
                      </div>
                  </div>
                </div>
      );
    }
    const AudienceTab = () => {
      return (
        <div className="bodyContentTab">
          <p>
            Target your message to the intended audience by specifying some rules.
          </p>
          <ul className="list-rule">
            <li className="rule-item">
              <input className="audienceRuleItem" type="text" value="3" style={{ width: 30 }}/>
            </li>
            <li className="rule-item">
              <DropDown title="days" items={menu_days} defaultText="days"/>
            </li>
            <li className="rule-item">
              <DropDown title="days" items={menu_days} defaultText="after"/>
            </li>
            <li className="rule-item">
              <DropDown title="days" items={menu_days} defaultText="join program"/>
            </li>
            <li className="rule-item">
              <DropDown title="days" items={menu_days} defaultText="AND"/>
            </li>
            <li className="rule-item">
              <DropDown title="days" items={menu_days} defaultText="role"/>
            </li>
            <li className="rule-item">
              <DropDown title="days" items={menu_days} defaultText="equal to"/>
            </li>
            <li className="rule-item">
              <DropDown title="days" items={menu_days} defaultText="Coach"/>
            </li>
            <li className="rule-item">
              <DropDown title="days" items={menu_days} defaultText="ADD RULE"/>
            </li>
          </ul>
        </div>
      )
    }

    return (
      <Tabs>
        <Tab label="Name">
          <div className="bodyContentTab">

            <TextField hintText="Welcome Email" floatingLabelText="Title" fullWidth={true}/><br/>

          </div>
        </Tab>
        <Tab label="Audience">
          <AudienceTab />
        </Tab>
        <Tab label="Message">
          <MessageTab/>
        </Tab>
        <Tab label="Message">
          <DeliveryTab/>
        </Tab>
        <Tab label="Interaction">
          <InteractionTab/>
        </Tab>
      </Tabs>
    );
  }
}

const styles = {
  headline: {
    fontSize: 24,
    paddingTop: 16,
    marginBottom: 12,
    fontWeight: 400
  },
  toggle: {
    marginBottom: 16,
  }
};

export default TabsBuildingBlock;

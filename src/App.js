import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import logo from './logo.svg';
import './App.css';
import './Draft.css';
import getMuiTheme from 'material-ui/styles/getMuiTheme';
import {blue500} from 'material-ui/styles/colors';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import AutomationSettingComponent from './AutomationSettingComponent';
import Header from './Header';
import injectTapEventPlugin from 'react-tap-event-plugin';

const muiTheme = getMuiTheme({
  palette: {
    primary1Color: blue500,
  },
});
injectTapEventPlugin();
class App extends Component {
  render() {
    return (
      <div>
      <MuiThemeProvider muiTheme={muiTheme}>
        <Header />
      </MuiThemeProvider>
      <MuiThemeProvider muiTheme={muiTheme}>
          <AutomationSettingComponent />
      </MuiThemeProvider>
    </div>
    );
  }
}

export default App;

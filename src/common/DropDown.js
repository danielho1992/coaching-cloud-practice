import React, { Component } from 'react';
import DropDownMenu from 'material-ui/DropDownMenu';
import MenuItem from 'material-ui/MenuItem';

class DropDown extends Component {
  constructor(){
    super();
    this.state = {
       value : null,
       items : undefined,
    };
  }

  handleOpen = () => {
      this.setState({open: true});
  };
  handleChange = (event, index, value) => this.setState({value});
  componentWillMount() {
    this.setState({ items : this.props.items })
  }
  render() {
    return(
      <DropDownMenu
       value={this.state.value}
       onChange={this.handleChange.bind(this)}
     >
       <MenuItem value={null} primaryText={this.props.defaultText} />
       {this.state.items.map((item, index) =>
           <MenuItem key={index} value={index} primaryText={item} />
         )}
     </DropDownMenu>
    )
  }
}

export default DropDown;

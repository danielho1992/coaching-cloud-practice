import React, { Component } from 'react';
import { Tab, Tabs } from 'material-ui/Tabs';
import Dialog from 'material-ui/Dialog';
import FlatButton from 'material-ui/FlatButton';
import RaisedButton from 'material-ui/RaisedButton';
import TabsBuildingBlock from './TabsBuildingBlock';
import {blue500} from 'material-ui/styles/colors';
import muiThemeable from 'material-ui/styles/muiThemeable';
import FontIcon from 'material-ui/FontIcon';

class AutomationSettingComponent extends Component {

  constructor(){
    super();
    this.state = {
      open: false,
    };
  }

  handleOpen = () => {
      this.setState({open: true});
  };
  handleNext = () => {
        this.setState({open: false});
  };

  render() {
    const actions = [
     <RaisedButton
       label="SAVE"
       primary={true}
       onTouchTap={this.handleNext}
     />,
   ];
    return(
      <div>
      <Tabs className="tab-content">
        <Tab label="Activity">
          <div>
            <h2 style={styles.headline}>Activity</h2>

          </div>
        </Tab>
        <Tab label="Member" >
          <div>
            <h2 style={styles.headline}>Tab Two</h2>
            <p>
              This is another example tab.
            </p>
          </div>
        </Tab>
        <Tab
          label="Automation"
          data-route="/home"
        >
          <div>
            <RaisedButton icon={<FontIcon className="muidocs-icon-custom-github" />} label="Send email" onTouchTap={this.handleOpen} />
            <Dialog
              actions={actions}
              contentStyle={styles.customContentStyle}
              title="Send email"
              modal={true}
              open={this.state.open}
              bodyStyle = {{ padding: 0}}
              titleStyle = {{ backgroundColor : blue500, color: '#fff' }}
            >
                <TabsBuildingBlock />
            </Dialog>
          </div>
        </Tab>
        <Tab
          label="Message"
        >

        </Tab>
      </Tabs>
    </div>
    );
  }
}

const styles = {
  headline: {
    fontSize: 24,
    paddingTop: 16,
    marginBottom: 12,
    fontWeight: 400,
  },
  customContentStyle : {
  backgroundColor : blue500,
   padding: 0,
  }
};

export default AutomationSettingComponent;

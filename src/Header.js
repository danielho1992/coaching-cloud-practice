import React, { Component } from 'react';
import { Tab, Tabs } from 'material-ui/Tabs';
import TextField from 'material-ui/TextField';
import {blue500} from 'material-ui/styles/colors';
import muiThemeable from 'material-ui/styles/muiThemeable';
import AppBar from 'material-ui/AppBar';

class Header extends Component {

  constructor(){
    super();
    this.state = {
      open: false,
    };
  }

  handleOpen = () => {
      this.setState({open: true});
  };
  handleNext = () => {
        this.setState({open: false});
  };

  render() {
    return(
      <AppBar style={{ height: 112 }} titleStyle={{ display: 'none'}}>
        <TextField
          className="searchbar"
          underlineShow={false}
          style={{ maginTop: 8 }}
          inputStyle={styles.textStyle}
         />
      </AppBar>
    );
  }
}

const styles = {
  textStyle: {
    fontSize: 20,
    fontWeight: 400,
    borderWidth: 1,
    padding: 5,
    backgroundColor: 'rgba(255, 255, 255, 0.36)',

  },
};
const customContentStyle = {
backgroundColor : blue500,  padding: 0,
}
export default Header;
